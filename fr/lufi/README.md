# Framadrop

[Framadrop](https://framadrop.org) est un service en ligne libre qui permet de partager des fichiers de manière confidentielle et sécurisée.

  1. Si besoin, définissez la durée de conservation en ligne.
  2. Collez le fichier à transmettre.
  3. Partagez ensuite avec vos correspondants le lien qui vous est donné.

Vos fichiers sont chiffrés et stockés sur nos serveurs sans qu’il nous soit possible de les déchiffrer.

Le service repose sur le logiciel libre [Lufi](https://lufi.io).


---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/988l.jpg" height="340" width="570">
      <source src="https://framatube.org/blip/framadrop.mp4" type="video/mp4">
      <source src="https://framatube.org/blip/framadrop.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framadrop.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/)