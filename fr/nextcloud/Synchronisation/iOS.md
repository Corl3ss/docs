# Synchronisation sur iOS
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

## Agendas
* Ouvrez l'application **Paramètres**.
* Sélectionnez **Mail, Contacts, Calendrier**.
* Sélectionnez **Ajouter un compte...** dans la rubrique Mail.
* Sélectionnez **Autre**
* Sélectionnez **Ajouter un compte CalDAV**.
* Entrez les détails de votre compte.

    * Note : Si vous utilisez [l'authentification en deux étapes](../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa), vous devez créer [un mot de passe d'application](../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication).
    * Pour le serveur, entrez l'URL du type `https://framagenda.org/remote.php/dav/principals/users/utilisateur/` visible dans les paramètres de Framagenda (en bas à gauche).
    ![Adresse CalDAV iOS](../images/agenda-5.png)
    * Entrez votre nom d'utilisateur et votre mot de passe.
    * Sélectionnez **Suivant**.
* Si les informations sont correctes, sélectionnez **Terminé**.

Votre agenda sera maintenant synchronisé et visible dans l'application Calendrier.

## Contacts
* Ouvrez l'application **Paramètres**.
* Sélectionnez **Mail, Contacts, Calendrier**.
* Sélectionnez **Ajouter un compte...** dans la rubrique Mail.
* Sélectionnez **Autre**
* Sélectionnez **Ajouter un compte CardDAV**.
* De la même manière que pour les agendas, entrez les détails de votre compte.
* Si les informations sont correctes, sélectionnez **Terminé**.

Vos contacts seront maintenant synchronisés et visibles dans l'application Contacts.
