# Framacarte

[Framacarte](https://framacarte.org) est un service en ligne permettant de réaliser des cartes interactives personnalisées proposé par l'association Framasoft
en partenariat avec l’association [OpenStreetMap France](http://www.openstreetmap.fr)

Le service repose sur le logiciel libre [uMap](https://github.com/umap-project/umapl).
L'association [OpenStreetMap France](http://www.openstreetmap.fr) héberge également sur un de ses serveurs une *instance* du logiciel uMap : [http://umap.openstreetmap.fr/](https://umap.openstreetmap.fr/).

Voici une documentation réalisée par [Carto'Cité](http://cartocite.fr) pour vous aider à l'utiliser.
Initialement ces tutoriels sont publiés sur le [wiki de Carto'Cité](http://wiki.cartocite.fr/doku.php?id=umap:tutoriel_umap) et sont sous licence [Creative commons By-SA](http://creativecommons.org/licenses/by-sa/3.0/).

**[Carto'Cité](http://cartocite.fr)** propose plusieurs [formations professionnelles à OpenStreetMap](http://cartocite.fr/formations-openstreetmap/).
La **[formation uMap](http://cartocite.fr/formation-umap/)** de deux jours permet de maîtriser ses nombreuses fonctionnalités.

---

# Tutoriels : créer une carte personnalisée avec uMap

[![](images/umap.png)](https://umap.openstreetmap.fr/fr/)
Avec **uMap** vous pouvez positionner vos contacts sur une carte, valoriser un territoire, préparer la logistique d'un évènement, raconter vos vacances, etc.
 Une carte uMap peut être intégrée à une page Web, comme la carte ci-dessous.

<iframe style="margin-left:80px;border:solid 1px;" width="80%" height="200px" frameBorder="0" src="https://umap.openstreetmap.fr/en/map/cartocrise-culture-francaise-tu-te-meurs_26647?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=false&datalayersControl=false&onLoadPanel=none&captionBar=false#7/43.477/4.581"></iframe>

## uMap : un logiciel libre et cloud

Umap est un logiciel libre, ou open source. Cela signifie que son code peut être utilisé, copié et modifié par tous.
Le code source de umap est accessible sur ce [dépôt Github](https///github.com/umap-project/umap).

uMap est un logiciel "cloud" : il ne s'exécute pas sur votre ordinateur mais sur un serveur auquel vous accèdez par le réseau internet.
Les cartes sont stockées sur ce serveur, elles sont créées et consultées en utilisant un navigateur Web.

Vous pouvez l'utiliser gratuitement pour créer autant de cartes que vous voulez sans limitation de durée.

Page wiki sur uMap (en anglais) : http://wiki.openstreetmap.org/wiki/UMap

## Exemples de cartes uMap

Voici quelques cartes illustrant la variété des usages et la richesse des fonctionnalités de uMap :

 | Carte                                                                                                                           | Remarques                                                                                |
 | -----                                                                                                                           | ---------                                                                                |
 | [CartoCrise](http://umap.openstreetmap.fr/en/map/cartocrise-culture-francaise-tu-te-meurs_26647) (extrait visible ci-dessus)    | Carte des évènements culturels annulés, reprise par tous les grands médias           |
 | [L'Ile de France en Bandes Dessinées](http://umap.openstreetmap.fr/en/map/lile-de-france-en-bandes-dessinees_26576)             | Grand choix de fonds de cartes et possibilité d'intégrer des photos                    |
 | [La guerre civile en Syrie](http://umap.openstreetmap.fr/en/map/desyracuse-syria-civil-war-21-april-2015_37133#7/34.339/39.749) | Véritable travail de géographe pour documenter l'évolution de la guerre en Syrie      |
 | [Festival des 3 Continents](http://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381)                                | Intégration d'une vidéo et import dynamique des points de vélo-partage                |
 | [Parcs d'activités de Josselin](http://www.josselin-communaute.fr/Vie-economique/Parcs-d-Activites)                             | Cartes intégrées dans un site Web avec liens vers une page ... ou vers une autre carte |
 | [Nantes à vélo](http://umap.openstreetmap.fr/fr/map/nantes-a-velo_26382)                                                        | Affichage des données sous forme de carte de chaleur (ou de clusters)                   |
 | [Ski de randonnée au Maupas](http://umap.openstreetmap.fr/fr/map/rando-ski-au-maupas_31935)                                     | Fonction diaporama pour raconter ses vacances                                            |
 | [Tour de France 2015](http://umap.openstreetmap.fr/fr/search/?q=tour+de+france+2015)                                            | Recherche parmi les cartes créées par d'autres utilisateurs                            |
 | [ Alternatiba Nantes 2015](http://umap.openstreetmap.fr/fr/map/alternatiba-nantes-2015_30425)                                   | Grand nombre de calques pour organiser la logistique d'un événement                    |
 | [ Aménagements des bords de Loire](http://umap.openstreetmap.fr/fr/map/amenagement-des-bords-loire-a-nantes_24994)              | Fond de carte personnalisé pour une proposition d'aménagement                          |
 | [ Tourisme à Bologne](http://umap.openstreetmap.fr/fr/map/luoghi-turistici-e-personaggi-a-bologna_30043)                        | Import de données d'un tableur et pictogrammes personnalisés                           |
 | [ Hémicycle Ile-de-France](http://umap.openstreetmap.fr/fr/map/hemicycle-idf_70859)                                             | Un fond de carte très original pour une carte politique                                 |


## Maîtriser uMap en 12 leçons

Nous vous proposons d'apprendre à utiliser uMap par une série de 12 tutoriels répartis en 3 niveaux.

### Niveau débutant

  - [1 - Je consulte une carte umap](1-consulter.html)
  - [2 - Je crée ma première carte umap](2-premiere-carte.html)
  - [3 - J'utilise un compte et crée une belle carte](3-utiliser-un-compte.html)
  - [4 - Je modifie et personnalise ma carte](4-personnaliser.html)

### Niveau intermédiaire

  - [5 - Je crée des infobulles multimédia](5-infobulles-multimédia.html)
  - [6 - Je structure ma carte avec des calques](6-calques.html)
  - [7 - Je publie ma carte et en contrôle l'accès](7-publication-et-droit-d-acces.html)
  - [8 - Le cas des polygones](8-polygones.html)

### Niveau avancé

  - [9 - Je crée une carte à partir d'un tableur](9-a-partir-d-un-tableur.html)
  - ~~10 - Je calcule un itinéraire et l'importe dans une carte~~
  - [11 - Je valorise les données OpenStreetMap avec uMap](11-valoriser-OpenStreetMap.html)
  - ~~12 - J'adapte le contenu de la carte au niveau de zoom~~


