# Organisation des conversations
_____

## Types de canaux

Les canaux sont utilisés pour organiser les discussions entre les différents sujets. Il y a trois types de canaux : les canaux publics, les groupes privés et les messages instantanés. 


#### Canaux publics

Les canaux publics sont ouverts à toute l'équipe. Les nouveaux membres de l'équipe sont automatiquement intégrés à deux canaux publics : *Town Square* (*Centre Ville*) et *Off-Topic* (*Hors-Sujet*).

#### Groupes privés

Les groupes privés sont destinés aux sujets sensibles et ne sont accessibles que pour les membres choisis de l'équipe. N'importe quel membre d'un groupe privé peut ajouter d'autres membres. Chacun peut choisir de quitter le groupe à n'importe quel moment, mais seuls le propriétaire du groupe ou un administrateur peuvent retirer d'autres membres.

#### Messages instantanés

Les messages instantanés permettent les conversations entre deux membres de l'équipe. Ces conversations ne sont visibles que par les membres de l'équipe concernés. L'indicateur de présence en ligne à coté des noms des membres de l'équipe indique le statut : en ligne (navigateur actif), absent (pas d'activité du navigateur depuis au moins 5 minutes) et hors ligne (navigateur fermé).

## Gérer les canaux

Les canaux peuvent être créés, rejoints, renommés, quittés et supprimés.

#### Créer un canal

Créez un canal public ou un groupe privé en sélectionnant le symbole **+** à coté de l'entête *Canaux* ou *Groupes privés* dans le panneau de gauche. Pour commencer un fil de discussion instantanée, cliquez sur **Plus** en dessous de *Messages instantanés*. Vous aurez alors la liste des membres de l'équipe à qui vous pouvez envoyer un message.

#### Rejoindre un canal


Cliquez sur **Plus...** en bas de la liste des *Canaux* pour afficher une liste des canaux publics que vous pouvez rejoindre. Pour rejoindre un groupe privé, vous devez être ajouté par un membre de ce groupe.


#### Renommer un canal

Cliquez sur le nom du canal, en haut du panneau central, pour accéder au menu déroulant, puis cliquez sur **Renommer le canal**. Les propriétaires de canal peuvent renommer les canaux publics et les groupes privés qu’ils ont créés. Les administrateurs d’équipe et les administrateurs du système peuvent renommer n’importe quel canal ou groupe.


#### Quitter un canal

Cliquez sur le nom du canal, en haut du panneau central, pour accéder au menu déroulant, puis cliquez sur **Quitter canal**. Un membre d’une équipe qui quitte un groupe privé doit être ajouté à nouveau par un membre du groupe s’il souhaite rejoindre le groupe à nouveau. Les membres d’une équipe ne recevront pas de notifications de mention dans des canaux dont ils ne sont pas membres.


#### Supprimer un canal

Cliquez sur le nom du canal, en haut du panneau central, pour accéder au menu déroulant, puis cliquez sur **Supprimer le canal**. Les propriétaires de canal peuvent supprimer les canaux publics et les groupes privés qu’ils ont créés. Les administrateurs d’équipe et les administrateurs du système peuvent supprimer n’importe quel canal ou groupe.

Quand un canal est supprimé, il n’est plus affiché dans l'interface utilisateur, mais une copie archivée est conservée sur le serveur, dans le cas où elle serait nécessaire par la suite pour des raisons d’audit. En conséquence, l’URL d’un nouveau canal ne peut pas être identique à l’URL d’un canal supprimé.


