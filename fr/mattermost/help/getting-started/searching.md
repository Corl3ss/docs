# Recherche

_____

La zone de recherche de Mattermost renvoie les résultats (insensibles à la casse) présents dans tous les canaux dont vous êtes membre. Utilisez des modificateurs pour préciser votre recherche. Cliquez sur **Lien (jump)** à droite d'un résultat pour voir ce message dans l’archive du canal.

## Modificateurs de recherche


#### From: et In:


Utilisez `from:` pour rechercher des messages d'un utilisateur spécifique et `in:` pour rechercher des messages dans des canaux spécifiques.

- Par exemple, la recherche `Mattermost in:centre-ville` renvoie uniquement les messages dans le canal « Centre-Ville » qui contiennent `Mattermost`.


#### "Guillemets"


Utilisez des guillemets anglais (double quote : ") pour faire une recherche sur des termes exacts.


- Par exemple, la recherche `"site web Mattermost"` renvoie les messages qui contiennent la phrase entière `site web Mattermost`, et pas les messages qui contiennent seulement `site`, `web` ou `Mattermost`.

#### Métacaractère*

Utilisez le caractère `*` pour faire des recherches avec des parties de mots.

- Par exemple, la recherche `mat*` renvoie les messages qui contiennent `mattermost`, `matière` et d'autres mots qui commencent par `mat`.


## Hashtags

Les hashtags (ou « mots-dièse ») sont des libellés de messages qui facilitent la recherche. Recherchez tous les messages qui contiennent un hashtag en cliquant sur ce hashtag dans un message* existant, ou en tapant le hashtag avec le symbole dièse (`#`) dans la barre de recherche. Créez un hashtag dans un message en utilisant le symbole dièse (`#`) suivi de caractères alphanumériques.

Un hashtag valide :
    
- doit commencer par une lettre,

- doit finir par un caractère alphanumérique (lettre ou chiffre),

- doit contenir au moins 3 caractères, sans compter le `#`,

- peut contenir des points, des tirets ou des underscores (`_`).

Exemples :
    
`#bug`, `#marketing`, `#v2.1`, `#test_utilisateur`, `#po.int`, `#mot-cle`

Les hashtags ne sont pas des liens vers les canaux. Par exemple, s’il existe un canal nommé « Marketing », cliquer sur un hashtag `#marketing` ne vous redirige pas vers ce canal.

## Notes sur la recherche et problèmes connus

- Plusieurs termes de recherches sont connectés par «&nbsp;OU&nbsp;» par défaut. La recherche `site web Mattermost` renvoie les résultats qui contiennent «&nbsp;site&nbsp;» ou «&nbsp;web&nbsp;» ou «&nbsp;Mattermost&nbsp;».

- La recherche dans Mattermost utilise les fonctionnalités de recherche plein texte existantes dans une base de données MySQL ou Postgres.

- Les installations de Mattermost qui nécessitent que la recherche fonctionne dans les langages chinois, japonais ou coréen doivent utiliser MySQL 5.7.6+ et la configuration du [parseur plein texte ngram](https://dev.mysql.com/doc/refman/5.7/en/fulltext-search-ngram.html). Voir la [discussion sur CJK](https://github.com/mattermost/platform/issues/2033#issuecomment-183872616) pour plus amples détails.

- Cas non supportés :
    
    - Les « mots vides » (« le », « un », « mon »...) ne renvoient aucun résultat car ces mots sont utilisés trop fréquemment. Voir la liste complète des mots vides (en anglais) pour les bases de données [MySQL](http://dev.mysql.com/doc/refman/5.7/en/fulltext-stopwords.html) et [Postgres](http://apt-browse.org/browse/ubuntu/precise/main/i386/postgresql-9.1/9.1.3-2/file/usr/share/postgresql/9.1/tsearch_data/english.stop).
    
    - Les recherches de deux lettres ne renvoient aucun résultat.
    

- Problèmes connus :
    
    - Bases de données Postgres et MySQL :

        - Les recherches avec un métacaractère `*` ne renvoient aucun résultat.

        - Les caractères chinois peuvent renvoyer des correspondances inexactes. Essayez d’ajouter `*` à la fin de la recherche pour lancer une recherche générique.

        - Les adresses IP, par exemple, `10.100.200.101`, ne renvoient aucun résultat.
        
    - Bases de données Postgres :
        
        - Les phrases entre guillemets (`"`) renvoient les résultats pour les mots de la phrase pris individuellement.

        - Les deux-points (`:`) entre guillemets provoquent une erreur.

        - Les adresses de courriel ne renvoient aucun résultat.
        
    - Bases de données MySQL :

        - Les hashtags et les mentions récentes de noms d’utilisateur qui contiennent un point ne renvoient aucun résultat.
        
        
