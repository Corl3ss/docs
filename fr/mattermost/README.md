# Framateam

[Framateam](https://framateam.org) est un service de *tchat* libre qui permet de communiquer avec son équipe en notifiant ses collègues, de conserver ses conversations et d'y faire des recherches.

Le service repose sur le logiciel libre [Mattermost](http://mattermost.org).

Pour mieux comprendre ses fonctionnalités et les usages possibles, voici [un exemple d'utilisation](exemple-d-utilisation.html)

Voici une documentation pour vous aider à mettre en page vos messages, utiliser des emoticônes, partager des images et documents et maîtriser Framateam.
Il s’agit de la traduction française de la [documentation officielle](https://docs.mattermost.com) réalisé par l'équipe [Framalang](https://participer.framasoft.org/traduction-rejoignez-framalang/)

---

## Table des matières

### Premiers pas
*   [Identification](help/getting-started/signing-in.html)
*   [Bases de la messagerie](help/getting-started/messaging-basics.html)
*   [Configuration des notifications](help/getting-started/configuring-notifications.html)
*   [Organisation des conversations](help/getting-started/organizing-conversations.html)
*   [Recherche](help/getting-started/searching.html)
*   [Création d’équipes](help/getting-started/creating-teams.html)
*   [Gestion des membres](help/getting-started/managing-members.html)
### Messagerie
*   [Envoi de messages](help/messaging/sending-messages.html)
*   [Mentionner des correspondants/amis](help/messaging/mentioning-teammates.html)
*   [Formater le texte](help/messaging/formatting-text.html)
*   [Joindre des fichiers](help/messaging/attaching-files.html)
*   [Exécuter des commandes](help/messaging/executing-commands.html)
### Paramètres
*   [Paramètres du compte](help/settings/account-settings.html)
*   [Couleurs du thème](help/settings/theme-colors.html)
*   [Paramètres du canal](help/settings/channel-settings.html)
*   [Paramètres d’équipe](help/settings/team-settings.html)
